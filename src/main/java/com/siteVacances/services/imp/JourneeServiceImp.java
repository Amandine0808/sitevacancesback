package com.siteVacances.services.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siteVacances.entities.Journee;
import com.siteVacances.entities.Voyage;
import com.siteVacances.repository.JourneeRepository;
import com.siteVacances.services.JourneeService;

@Service
public class JourneeServiceImp implements JourneeService {
	@Autowired
	JourneeRepository journeeRepository;
	
	@Override
	public List<Journee> findAll() {
		// TODO Auto-generated method stub
		return journeeRepository.findAll();
	}

	@Override
	public Optional<Journee> findOne(Long id) {
		// TODO Auto-generated method stub
		return journeeRepository.findById(id);
	}

	@Override
	public Journee save(Journee journee) {
		// TODO Auto-generated method stub
		return journeeRepository.save(journee);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		journeeRepository.deleteById(id);
	}

	@Override
	public List<Journee> findJourneesOrdreChrono() {
		// TODO Auto-generated method stub
		return journeeRepository.findJourneesOrdreChrono();
	}

	@Override
	public List<Journee> findJourneesByVoyage(Long idVoyage) {
		// TODO Auto-generated method stub
		return journeeRepository.findJourneesByVoyage(idVoyage);
	}

}
