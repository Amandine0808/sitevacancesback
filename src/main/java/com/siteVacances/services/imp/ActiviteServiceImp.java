package com.siteVacances.services.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siteVacances.entities.Activite;
import com.siteVacances.repository.ActiviteRepository;
import com.siteVacances.services.ActiviteService;

@Service
public class ActiviteServiceImp implements ActiviteService {
	@Autowired
	ActiviteRepository activiteRepository;

	@Override
	public List<Activite> findAll() {
		// TODO Auto-generated method stub
		return activiteRepository.findAll();
	}

	@Override
	public Optional<Activite> findOne(Long id) {
		// TODO Auto-generated method stub
		return activiteRepository.findById(id);
	}

	@Override
	public Activite save(Activite activite) {
		// TODO Auto-generated method stub
		return activiteRepository.save(activite);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		activiteRepository.deleteById(id);
	}

	@Override
	public List<Activite> findActivitesByJournee(Long idJournee) {
		// TODO Auto-generated method stub
		return activiteRepository.findActivitesByJournee(idJournee);
	}

}
