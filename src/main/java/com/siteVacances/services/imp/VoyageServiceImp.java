package com.siteVacances.services.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siteVacances.entities.Voyage;
import com.siteVacances.repository.VoyageRepository;
import com.siteVacances.services.VoyageService;

@Service
public class VoyageServiceImp implements VoyageService {
	@Autowired
	VoyageRepository voyageRepository;

	@Override
	public List<Voyage> findAll() {
		// TODO Auto-generated method stub
		return voyageRepository.findAll();
	}

	@Override
	public Optional<Voyage> findOne(Long id) {
		// TODO Auto-generated method stub
		return voyageRepository.findById(id);
	}

	@Override
	public Voyage save(Voyage voyage) {
		// TODO Auto-generated method stub
		return voyageRepository.save(voyage);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		voyageRepository.deleteById(id);
	}

	@Override
	public Voyage findLastVoyage() {
		// TODO Auto-generated method stub
		return voyageRepository.findLastVoyage();
	}

	@Override
	public List<Voyage> findVoyagesOrdreChrono() {
		// TODO Auto-generated method stub
		return voyageRepository.findVoyagesOrdreChrono();
	}

}
