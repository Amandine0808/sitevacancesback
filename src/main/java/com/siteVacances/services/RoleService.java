package com.siteVacances.services;

import java.util.List;
import java.util.Optional;

import com.siteVacances.entities.Role;

public interface RoleService {
	List<Role> findAll();

	Optional<Role> findOne(Long id);

	Role save(Role role);

	void delete(Long id);

}
