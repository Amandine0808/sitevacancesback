package com.siteVacances.services;

import java.util.List;
import java.util.Optional;

import com.siteVacances.entities.User;

public interface UserService {
	List<User> findAll();

	Optional<User> findOne(Long id);

	User save(User user);

	void delete(Long id);
	
	Optional<User> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
}
