package com.siteVacances.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.siteVacances.entities.Journee;
import com.siteVacances.entities.Voyage;

public interface JourneeService {
	List<Journee> findAll();

	Optional<Journee> findOne(Long id);

	Journee save(Journee journee);

	void delete(Long id);
	
	List<Journee> findJourneesOrdreChrono();
	
	List<Journee> findJourneesByVoyage(Long idVoyage);
}
