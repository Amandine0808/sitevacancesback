package com.siteVacances.services;

import java.util.List;
import java.util.Optional;

import com.siteVacances.entities.Voyage;

public interface VoyageService {
	List<Voyage> findAll();

	Optional<Voyage> findOne(Long id);

	Voyage save(Voyage voyage);

	void delete(Long id);

	Voyage findLastVoyage();

	List<Voyage> findVoyagesOrdreChrono();
}
