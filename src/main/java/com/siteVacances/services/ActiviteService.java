package com.siteVacances.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.siteVacances.entities.Activite;

public interface ActiviteService {
	List<Activite> findAll();

	Optional<Activite> findOne(Long id);

	Activite save(Activite activite);

	void delete(Long id);
	
	List<Activite> findActivitesByJournee(Long idJournee);
}
