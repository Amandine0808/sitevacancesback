package com.siteVacances.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Table
@Entity
public class Activite implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ACTIVITE")
	private Long idActivite;
	@Column(name = "DESCRIPTIF")
	private String descriptif;
	@Column(name = "PHOTO")
	private String photo;
	@Column(name = "NOM_PHOTO_ACTIVITE")
	private String nomPhotoActivite;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "FK_JOURNEE", referencedColumnName = "ID_JOURNEE")
	private Journee journee;

	public Activite() {
		super();
	}

	public Activite(String descriptif, String photo, String nomPhotoActivite) {
		super();
		this.descriptif = descriptif;
		this.photo = photo;
		this.nomPhotoActivite = nomPhotoActivite;
	}

	public Activite(Long idActivite, String descriptif, String photo, String nomPhotoActivite) {
		super();
		this.idActivite = idActivite;
		this.descriptif = descriptif;
		this.photo = photo;
		this.nomPhotoActivite = nomPhotoActivite;
	}

	public Activite(String descriptif, String photo, String nomPhotoActivite, Journee journee) {
		super();
		this.descriptif = descriptif;
		this.photo = photo;
		this.nomPhotoActivite = nomPhotoActivite;
		this.journee = journee;
	}

	public Activite(Long idActivite, String descriptif, String photo, String nomPhotoActivite, Journee journee) {
		super();
		this.idActivite = idActivite;
		this.descriptif = descriptif;
		this.photo = photo;
		this.nomPhotoActivite = nomPhotoActivite;
		this.journee = journee;
	}

	public Long getIdActivite() {
		return idActivite;
	}

	public void setIdActivite(Long idActivite) {
		this.idActivite = idActivite;
	}

	public String getDescriptif() {
		return descriptif;
	}

	public void setDescriptif(String descriptif) {
		this.descriptif = descriptif;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getNomPhotoActivite() {
		return nomPhotoActivite;
	}

	public void setNomPhotoActivite(String nomPhotoActivite) {
		this.nomPhotoActivite = nomPhotoActivite;
	}

	public Journee getJournee() {
		return journee;
	}

	public void setJournee(Journee journee) {
		this.journee = journee;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descriptif == null) ? 0 : descriptif.hashCode());
		result = prime * result + ((idActivite == null) ? 0 : idActivite.hashCode());
		result = prime * result + ((journee == null) ? 0 : journee.hashCode());
		result = prime * result + ((nomPhotoActivite == null) ? 0 : nomPhotoActivite.hashCode());
		result = prime * result + ((photo == null) ? 0 : photo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Activite other = (Activite) obj;
		if (descriptif == null) {
			if (other.descriptif != null)
				return false;
		} else if (!descriptif.equals(other.descriptif))
			return false;
		if (idActivite == null) {
			if (other.idActivite != null)
				return false;
		} else if (!idActivite.equals(other.idActivite))
			return false;
		if (journee == null) {
			if (other.journee != null)
				return false;
		} else if (!journee.equals(other.journee))
			return false;
		if (nomPhotoActivite == null) {
			if (other.nomPhotoActivite != null)
				return false;
		} else if (!nomPhotoActivite.equals(other.nomPhotoActivite))
			return false;
		if (photo == null) {
			if (other.photo != null)
				return false;
		} else if (!photo.equals(other.photo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Activite [idActivite=" + idActivite + ", descriptif=" + descriptif + ", photo=" + photo
				+ ", nomPhotoActivite=" + nomPhotoActivite + ", journee=" + journee + "]";
	}

}
