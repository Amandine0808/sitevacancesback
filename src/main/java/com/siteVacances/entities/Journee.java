package com.siteVacances.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Table
@Entity
public class Journee implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_JOURNEE")
	private Long idJournee;
	@Column(name = "DATE_JOURNEE")
	private Date dateJournee;
	@Column(name = "HEBERGEMENT")
	private String hebergement;
	@Column(name = "INDEX_JOURNEE")
	private Long indexJournee;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "FK_VOYAGE", referencedColumnName = "ID_VOYAGE")
	private Voyage voyage;
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE }, fetch = FetchType.LAZY, mappedBy = "journee")
	@JsonManagedReference
	private List<Activite> activites;

	public Journee() {
		super();
	}

	public Journee(Date dateJournee, String hebergement, Long indexJournee) {
		super();
		this.dateJournee = dateJournee;
		this.hebergement = hebergement;
		this.indexJournee = indexJournee;

	}

	public Journee(Long idJournee, Date dateJournee, String hebergement, Long indexJournee) {
		super();
		this.idJournee = idJournee;
		this.dateJournee = dateJournee;
		this.hebergement = hebergement;
		this.indexJournee = indexJournee;
	}

	public Journee(Date dateJournee, String hebergement, Long indexJournee, Voyage voyage) {
		super();
		this.dateJournee = dateJournee;
		this.hebergement = hebergement;
		this.indexJournee = indexJournee;
		this.voyage = voyage;
	}

	public Journee(Long idJournee, Date dateJournee, String hebergement, Long indexJournee, Voyage voyage) {
		super();
		this.idJournee = idJournee;
		this.dateJournee = dateJournee;
		this.hebergement = hebergement;
		this.indexJournee = indexJournee;
		this.voyage = voyage;
	}

	public Journee(Date dateJournee, String hebergement, Long indexJournee, List<Activite> activites) {
		super();
		this.dateJournee = dateJournee;
		this.hebergement = hebergement;
		this.indexJournee = indexJournee;
		this.activites = activites;
	}

	public Journee(Long idJournee, Date dateJournee, String hebergement, Long indexJournee, List<Activite> activites) {
		super();
		this.idJournee = idJournee;
		this.dateJournee = dateJournee;
		this.hebergement = hebergement;
		this.indexJournee = indexJournee;
		this.activites = activites;
	}

	public Journee(Date dateJournee, String hebergement, Long indexJournee, Voyage voyage, List<Activite> activites) {
		super();
		this.dateJournee = dateJournee;
		this.hebergement = hebergement;
		this.indexJournee = indexJournee;
		this.voyage = voyage;
		this.activites = activites;
	}

	public Journee(Long idJournee, Date dateJournee, String hebergement, Long indexJournee, Voyage voyage,
			List<Activite> activites) {
		super();
		this.idJournee = idJournee;
		this.dateJournee = dateJournee;
		this.hebergement = hebergement;
		this.indexJournee = indexJournee;
		this.voyage = voyage;
		this.activites = activites;
	}

	public Long getIdJournee() {
		return idJournee;
	}

	public void setIdJournee(Long idJournee) {
		this.idJournee = idJournee;
	}

	public Date getDateJournee() {
		return dateJournee;
	}

	public void setDateJournee(Date dateJournee) {
		this.dateJournee = dateJournee;
	}

	public String getHebergement() {
		return hebergement;
	}

	public void setHebergement(String hebergement) {
		this.hebergement = hebergement;
	}

	public Long getIndexJournee() {
		return indexJournee;
	}

	public void setIndexJournee(Long indexJournee) {
		this.indexJournee = indexJournee;
	}

	public Voyage getVoyage() {
		return voyage;
	}

	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}

	public List<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activites == null) ? 0 : activites.hashCode());
		result = prime * result + ((dateJournee == null) ? 0 : dateJournee.hashCode());
		result = prime * result + ((hebergement == null) ? 0 : hebergement.hashCode());
		result = prime * result + ((idJournee == null) ? 0 : idJournee.hashCode());
		result = prime * result + ((indexJournee == null) ? 0 : indexJournee.hashCode());
		result = prime * result + ((voyage == null) ? 0 : voyage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Journee other = (Journee) obj;
		if (activites == null) {
			if (other.activites != null)
				return false;
		} else if (!activites.equals(other.activites))
			return false;
		if (dateJournee == null) {
			if (other.dateJournee != null)
				return false;
		} else if (!dateJournee.equals(other.dateJournee))
			return false;
		if (hebergement == null) {
			if (other.hebergement != null)
				return false;
		} else if (!hebergement.equals(other.hebergement))
			return false;
		if (idJournee == null) {
			if (other.idJournee != null)
				return false;
		} else if (!idJournee.equals(other.idJournee))
			return false;
		if (indexJournee == null) {
			if (other.indexJournee != null)
				return false;
		} else if (!indexJournee.equals(other.indexJournee))
			return false;
		if (voyage == null) {
			if (other.voyage != null)
				return false;
		} else if (!voyage.equals(other.voyage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Journee [idJournee=" + idJournee + ", dateJournee=" + dateJournee + ", hebergement=" + hebergement
				+ ", indexJournee=" + indexJournee + ", voyage=" + voyage + ", activites=" + activites + "]";
	}

}
