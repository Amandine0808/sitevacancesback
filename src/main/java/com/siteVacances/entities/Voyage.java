package com.siteVacances.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Table
@Entity
public class Voyage implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_VOYAGE")
	private Long idVoyage;
	@Column(name = "PLUSIEURS_JOURS")
	private boolean plusieursJours;
	@Column(name = "DATE_DEBUT")
	private Date dateDebut;
	@Column(name = "DATE_FIN")
	private Date dateFin;
	@Column(name = "LIEU")
	private String lieu;
	@Column(name = "PHOTO")
	private String photo;
	@Column(name = "NOM_PHOTO_VOYAGE")
	private String nomPhotoVoyage;
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.EAGER, mappedBy = "voyage")
	@JsonManagedReference
	private List<Journee> journees;

	public Voyage() {
		super();
	}

	public Voyage(boolean plusieursJours, Date dateDebut, Date dateFin, String lieu, String photo,
			String nomPhotoVoyage) {
		super();
		this.plusieursJours = plusieursJours;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
		this.photo = photo;
		this.nomPhotoVoyage = nomPhotoVoyage;
	}

	public Voyage(Long idVoyage, boolean plusieursJours, Date dateDebut, Date dateFin, String lieu, String photo,
			String nomPhotoVoyage) {
		super();
		this.idVoyage = idVoyage;
		this.plusieursJours = plusieursJours;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
		this.photo = photo;
		this.nomPhotoVoyage = nomPhotoVoyage;
	}

	public Voyage(boolean plusieursJours, Date dateDebut, Date dateFin, String lieu, String photo,
			String nomPhotoVoyage, User user) {
		super();
		this.plusieursJours = plusieursJours;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
		this.photo = photo;
		this.nomPhotoVoyage = nomPhotoVoyage;
		this.user = user;
	}

	public Voyage(Long idVoyage, boolean plusieursJours, Date dateDebut, Date dateFin, String lieu, String photo,
			String nomPhotoVoyage, User user) {
		super();
		this.idVoyage = idVoyage;
		this.plusieursJours = plusieursJours;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
		this.photo = photo;
		this.nomPhotoVoyage = nomPhotoVoyage;
		this.user = user;
	}

	public Voyage(boolean plusieursJours, Date dateDebut, Date dateFin, String lieu, String photo,
			String nomPhotoVoyage, List<Journee> journees) {
		super();
		this.plusieursJours = plusieursJours;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
		this.photo = photo;
		this.nomPhotoVoyage = nomPhotoVoyage;
		this.journees = journees;
	}

	public Voyage(Long idVoyage, boolean plusieursJours, Date dateDebut, Date dateFin, String lieu, String photo,
			String nomPhotoVoyage, List<Journee> journees) {
		super();
		this.idVoyage = idVoyage;
		this.plusieursJours = plusieursJours;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
		this.photo = photo;
		this.nomPhotoVoyage = nomPhotoVoyage;
		this.journees = journees;
	}

	public Voyage(Long idVoyage, boolean plusieursJours, Date dateDebut, Date dateFin, String lieu, String photo,
			String nomPhotoVoyage, User user, List<Journee> journees) {
		super();
		this.idVoyage = idVoyage;
		this.plusieursJours = plusieursJours;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
		this.photo = photo;
		this.nomPhotoVoyage = nomPhotoVoyage;
		this.user = user;
		this.journees = journees;
	}

	public Long getIdVoyage() {
		return idVoyage;
	}

	public void setIdVoyage(Long idVoyage) {
		this.idVoyage = idVoyage;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public boolean isPlusieursJours() {
		return plusieursJours;
	}

	public void setPlusieursJours(boolean plusieursJours) {
		this.plusieursJours = plusieursJours;
	}

	public String getNomPhotoVoyage() {
		return nomPhotoVoyage;
	}

	public void setNomPhotoVoyage(String nomPhotoVoyage) {
		this.nomPhotoVoyage = nomPhotoVoyage;
	}

	public List<Journee> getJournees() {
		return journees;
	}

	public void setJournees(List<Journee> journees) {
		this.journees = journees;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateDebut == null) ? 0 : dateDebut.hashCode());
		result = prime * result + ((dateFin == null) ? 0 : dateFin.hashCode());
		result = prime * result + ((idVoyage == null) ? 0 : idVoyage.hashCode());
		result = prime * result + ((journees == null) ? 0 : journees.hashCode());
		result = prime * result + ((lieu == null) ? 0 : lieu.hashCode());
		result = prime * result + ((nomPhotoVoyage == null) ? 0 : nomPhotoVoyage.hashCode());
		result = prime * result + ((photo == null) ? 0 : photo.hashCode());
		result = prime * result + (plusieursJours ? 1231 : 1237);
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Voyage other = (Voyage) obj;
		if (dateDebut == null) {
			if (other.dateDebut != null)
				return false;
		} else if (!dateDebut.equals(other.dateDebut))
			return false;
		if (dateFin == null) {
			if (other.dateFin != null)
				return false;
		} else if (!dateFin.equals(other.dateFin))
			return false;
		if (idVoyage == null) {
			if (other.idVoyage != null)
				return false;
		} else if (!idVoyage.equals(other.idVoyage))
			return false;
		if (journees == null) {
			if (other.journees != null)
				return false;
		} else if (!journees.equals(other.journees))
			return false;
		if (lieu == null) {
			if (other.lieu != null)
				return false;
		} else if (!lieu.equals(other.lieu))
			return false;
		if (nomPhotoVoyage == null) {
			if (other.nomPhotoVoyage != null)
				return false;
		} else if (!nomPhotoVoyage.equals(other.nomPhotoVoyage))
			return false;
		if (photo == null) {
			if (other.photo != null)
				return false;
		} else if (!photo.equals(other.photo))
			return false;
		if (plusieursJours != other.plusieursJours)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Voyage [idVoyage=" + idVoyage + ", plusieursJours=" + plusieursJours + ", dateDebut=" + dateDebut
				+ ", dateFin=" + dateFin + ", lieu=" + lieu + ", photo=" + photo + ", nomPhotoVoyage=" + nomPhotoVoyage
				+ ", user=" + user + ", journees=" + journees + "]";
	}

}
