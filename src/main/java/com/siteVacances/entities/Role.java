package com.siteVacances.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity
public class Role implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_ROLE")
	private Long idRole;
	@Enumerated(EnumType.STRING)
	@Column(name="NOM")
	private EnumRole nom;
	@Column(name="DESCRIPTION")
	private String description;

	public Role() {
		super();
	}

	public Role(EnumRole nom, String description) {
		super();
		this.nom = nom;
		this.description = description;
	}

	public Role(Long id, EnumRole nom, String description) {
		super();
		this.idRole = id;
		this.nom = nom;
		this.description = description;
	}

	public Long getId() {
		return idRole;
	}

	public void setId(Long id) {
		this.idRole = id;
	}

	public EnumRole getNom() {
		return nom;
	}

	public void setNom(EnumRole nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (idRole ^ (idRole >>> 32));
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idRole != other.idRole)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Role [id=" + idRole + ", titre=" + nom + ", description=" + description + "]";
	}

}
