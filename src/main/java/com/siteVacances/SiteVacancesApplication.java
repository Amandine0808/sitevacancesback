package com.siteVacances;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiteVacancesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiteVacancesApplication.class, args);
	}

}
