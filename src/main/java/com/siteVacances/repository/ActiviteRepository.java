package com.siteVacances.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.siteVacances.entities.Activite;

@Repository
public interface ActiviteRepository extends JpaRepository<Activite, Long> {
	@Query("select a from Activite a where a.journee.idJournee= :journee")
	public List<Activite> findActivitesByJournee(@Param("journee")Long idJournee);
}
