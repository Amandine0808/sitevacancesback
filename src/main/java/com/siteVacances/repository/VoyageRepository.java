package com.siteVacances.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.siteVacances.entities.Voyage;

@Repository
public interface VoyageRepository extends JpaRepository<Voyage, Long> {
	@Query("from Voyage v where v.idVoyage =(select max(v.idVoyage) from Voyage v)")
	public Voyage findLastVoyage();

	@Query("select v from Voyage v order by v.dateDebut")
	public List<Voyage> findVoyagesOrdreChrono();
}
