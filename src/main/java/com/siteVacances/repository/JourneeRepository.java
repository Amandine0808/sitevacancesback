package com.siteVacances.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.siteVacances.entities.Journee;

@Repository
public interface JourneeRepository extends JpaRepository<Journee, Long> {
	@Query("select j from Journee j order by j.dateJournee")
	public List<Journee> findJourneesOrdreChrono();
	
	@Query("select j from Journee j where j.voyage.idVoyage= :voyage order by j.dateJournee")
	public List<Journee> findJourneesByVoyage(@Param("voyage")Long idVoyage);
}
