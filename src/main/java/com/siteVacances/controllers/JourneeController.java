package com.siteVacances.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siteVacances.entities.Journee;
import com.siteVacances.services.JourneeService;

@CrossOrigin
@RestController
public class JourneeController {
	@Autowired
	JourneeService js;
	
	@RequestMapping(value = "journees", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public List<Journee> findAllJournee() {
		return js.findAll();
	}

	@RequestMapping(value = "journees/{idJournee}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public Optional<Journee> findOneJournee(@PathVariable("idJournee") Long id) {
		return js.findOne(id);
	}

	@RequestMapping(value = "journees", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN')")
	public Journee saveJournee(@RequestBody Journee journee) {
		return js.save(journee);
	}

	@RequestMapping(value = "journees/{idJournee}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteJournee(@PathVariable("idJournee") Long id) {
		js.delete(id);
	}
	
	@RequestMapping(value = "journees/ordreChrono", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public List<Journee> findJourneesOrdreChrono() {
		return js.findJourneesOrdreChrono();
	}
	
	@RequestMapping(value = "journees/voyage{idVoyage}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public List<Journee> findJourneesByVoyage(@PathVariable("idVoyage") Long id) {
		return js.findJourneesByVoyage(id);
	}
}
