package com.siteVacances.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siteVacances.entities.Voyage;
import com.siteVacances.services.VoyageService;

@CrossOrigin
@RestController
public class VoyageController {
	@Autowired
	VoyageService vs;

	@RequestMapping(value = "voyages", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public List<Voyage> findAllVoyage() {
		return vs.findAll();
	}

	@RequestMapping(value = "voyages/{idVoyage}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public Optional<Voyage> findOneVoyage(@PathVariable("idVoyage") Long id) {
		return vs.findOne(id);
	}

	@RequestMapping(value = "voyages", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN')")
	public Voyage saveVoyage(@RequestBody Voyage voyage) {
		return vs.save(voyage);
	}

	@RequestMapping(value = "voyages/{idVoyage}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteVoyage(@PathVariable("idVoyage") Long id) {
		vs.delete(id);
	}
	
	@RequestMapping(value = "voyages/idMax", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public Voyage findLastVoyage() {
		return vs.findLastVoyage();
	}
	
	@RequestMapping(value = "voyages/ordreChrono", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public List<Voyage> findVoyagesOrdreChrono() {
		return vs.findVoyagesOrdreChrono();
	}
}
