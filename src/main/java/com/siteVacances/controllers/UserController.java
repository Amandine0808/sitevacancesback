package com.siteVacances.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siteVacances.entities.User;
import com.siteVacances.services.UserService;

@CrossOrigin
@RestController
public class UserController {
	@Autowired
	UserService us;

	@RequestMapping(value = "users", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMIN')")
	public List<User> findAllUser() {
		return us.findAll();
	}

	@RequestMapping(value = "users/{idUser}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMIN')")
	public Optional<User> findOneUser(@PathVariable("idUser") Long id) {
		return us.findOne(id);
	}

	@RequestMapping(value = "users", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN')")
	public User saveUser(@RequestBody User user) {
		return us.save(user);
	}

	@RequestMapping(value = "users/{idUser}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteUser(@PathVariable("idUser") Long id) {
		us.delete(id);
	}
}
