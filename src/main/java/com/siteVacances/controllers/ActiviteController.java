package com.siteVacances.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siteVacances.entities.Activite;
import com.siteVacances.services.ActiviteService;

@CrossOrigin
@RestController
public class ActiviteController {
	@Autowired
	ActiviteService as;
	
	@RequestMapping(value = "activites", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public List<Activite> findAllActivite() {
		return as.findAll();
	}

	@RequestMapping(value = "activites/{idActivite}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public Optional<Activite> findOneActivite(@PathVariable("idActivite") Long id) {
		return as.findOne(id);
	}

	@RequestMapping(value = "activites", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN')")
	public Activite saveActivite(@RequestBody Activite activite) {
		return as.save(activite);
	}

	@RequestMapping(value = "activites/{idActivite}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteActivite(@PathVariable("idActivite") Long id) {
		as.delete(id);
	}
	
	@RequestMapping(value = "activites/journee{idJournee}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public List<Activite> findActivitesByJournee(@PathVariable("idJournee") Long id) {
		return as.findActivitesByJournee(id);
	}
}
