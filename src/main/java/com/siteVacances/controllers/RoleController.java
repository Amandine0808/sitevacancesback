package com.siteVacances.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siteVacances.entities.Role;
import com.siteVacances.services.RoleService;

@CrossOrigin
@RestController
public class RoleController {
	@Autowired
	RoleService rs;

	@RequestMapping(value = "roles", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMIN')")
	public List<Role> findAllRole() {
		return rs.findAll();
	}

	@RequestMapping(value = "roles/{idRole}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMIN')")
	public Optional<Role> findOneRole(@PathVariable("idRole") Long id) {
		return rs.findOne(id);
	}

	@RequestMapping(value = "roles", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN')")
	public Role saveOrUpdateRole(@RequestBody Role role) {
		return rs.save(role);
	}

	@RequestMapping(value = "roles/{idRole}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteRole(@PathVariable("idRole") Long id) {
		rs.delete(id);
	}
}
