package com.siteVacances.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.siteVacances.security.services.UserDetailsServiceImp;

@SpringBootApplication //idem @Configuration : dit que cette classe configure Spring Security de façon personnalisée : elle sera scannée au démarrage. Est nécessaire pour prévenir Spring qu'il y a des Bean dans cette classe
@EnableWebSecurity //ici, cette annotation est facultative car on ne faut que re-écrire les méthodes de WebSecurityConfigurerAdapter. Elle serait obligatoire si on avait supprimé la configuration pas défaut de Spring Security. Dit que 
@EnableGlobalMethodSecurity(prePostEnabled = true) //Dit qu'on applique AOP security aux méthodes
public class Configuration extends WebSecurityConfigurerAdapter {
	@Autowired
	UserDetailsServiceImp userDetailsService ;
	
	@Autowired
	private AuthEntryPointJwt unauthorizedHandler;

	@Bean //Spring enregistre la valeur retournée en tant qu'un Bean(objet géré par Spring IOC)
	public AuthTokenFilter authenticationJwtTokenFilter() {
		return new AuthTokenFilter();
	}
	
	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception{ 
		authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable()
			.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.authorizeRequests().antMatchers("/api/auth/**").permitAll()
			.antMatchers("/api/test/**").permitAll()
			.anyRequest().authenticated();

		http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	
}
